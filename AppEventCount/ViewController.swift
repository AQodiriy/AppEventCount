
import UIKit

class ViewController: UIViewController {
    
    let stackView = UIStackView()
    
    let didFinishLaunchOptions = UILabel()
    let configurationForConnecting = UILabel()
    
    let willConnectTo = UILabel()
    let sceneDidBecomeActive = UILabel()
    let sceneWillResignActive = UILabel()
    let sceneWillEnterForeground = UILabel()
    let sceneDidEnterBackground = UILabel()
    
    var willConnectCount         = 0
    var didBecomeActiveCount     = 0
    var willResignActive         = 0
    var willEnterForegroundCount = 0
    var didEnterBackgroundCount       = 0
    
    
    var appDelegate = (UIApplication.shared.delegate as! AppDelegate)

    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set up stackView
        stackView.axis = .vertical
        stackView.spacing = 5  // Adjust the spacing between labels if needed
        stackView.translatesAutoresizingMaskIntoConstraints  = false
        view.addSubview(stackView)

        // Set up labels
        let labels = [didFinishLaunchOptions, configurationForConnecting, willConnectTo, sceneDidBecomeActive, sceneWillResignActive, sceneWillEnterForeground, sceneDidEnterBackground]

        for label in labels {
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textAlignment = .center
            label.numberOfLines = 0 // Set the number of lines to 0 for multiline labels
            stackView.addArrangedSubview(label)
        }

        // Set up constraints for stackView
        stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true

        // Set text for each label (modify as needed)
        updateView()
    }

    
    func updateView(){
        didFinishLaunchOptions.text = "The App has launched \(appDelegate.launchCount) time(s)"
        configurationForConnecting.text = "Configured for connecting \(appDelegate.configurationForConnectingCount) times"
        willConnectTo.text = "Will Connect \(willConnectCount) times"
        sceneDidBecomeActive.text = "Scene Did Become Active \(didBecomeActiveCount) times"
        sceneWillResignActive.text = "Scene Will Resign Active \(willResignActive) times"
        sceneWillEnterForeground.text = "Scene Will Enter Foreground \(willEnterForegroundCount) times"
        sceneDidEnterBackground.text = "Scene Did Enter Background \(didEnterBackgroundCount) times"
        
        
    }
}

